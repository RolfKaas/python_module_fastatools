import subprocess
import re
import argparse
import os.path
import hashlib
import json
import textwrap
import gzip
import sys
from itertools import groupby


class Metadata():
    ''' The main part of a Metadata object is the "data" dict. The keys are
        fasta headers and the values are dicts with the keys "sha1" and
        "length", containing the sha1 digest of the sequence and the length of
        the sequence, respectively.
        The object can either be created from a fasta file or a json file
        containing a dict/hash.

        data dict:
            Key: Fasta header
                Val: <internal dict>

        internal dict:
            Key: "sha1"
            Val: sha1 digest of fasta sequence without header, newlines, and
                 encoded in utf-8.

            Key: "length"
            Val: Length of fasta sequence without newlines.

            Key: "file"
            Val: File name in which the sequence is found.
    '''

    @staticmethod
    def fasta_iter(fasta_name):
        ''' Given a fasta file. yield tuples of header, sequence
        '''
        with open(fasta_name, "r", encoding="utf-8") as fh:
            # TRASH: fh = open(fasta_name)
            # Tech note-lambda syntax: lambda args:expression
            # Tech note-groupby syntax: groupby(iterable, key=None)
            #   returns iterator with groups defined by key func.
            # Create an iterator containing: header, seq, header, seq ...
            # Ditching boolean x[0] but keeping the header or sequence in x[1]
            # since we know they alternate.
            faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))

            # Reg exp: match from start of string up until the first whitespace
            #          If no whitespace is found the entire string is mathced.
            re_header_wo_ws = re.compile(r"^(\S+)")

            for header in faiter:
                # drop the ">"
                headerStr = header.__next__()[1:].strip()
                re_header_match = re_header_wo_ws.search(headerStr)
                headerStr = re_header_match.group(1)
                # join all sequence lines to one.
                seq = "".join(s.strip() for s in faiter.__next__())
                if(seq[0] == ">"):
                    print("New header found where sequence was expected. If an\
                    entry is empty, it must contain an empty line following\
                    the corresponding header.")
                    quit(1)
                yield (headerStr, seq)

    def __init__(self, fasta=False, json_file=False, duplicates_allowed=False):
        ''' Constructor.
            Must be provided with either a list of fasta files or a json
            formatted file.
        '''
        self.data = {}
        self.duplicates = {}

        if(not fasta and not json_file):
            print("Metadata Class was initiated without a path to one or more\
                   fasta files or a json formatted file. One or the other is\
                   required.")
            quit(1)

        if(fasta):
            # Load dict from each FASTA file.
            for fasta_file in fasta:
                fasta_generator = self.fasta_iter(fasta_file)
                for entry in fasta_generator:
                    header, seq = entry
                    hash_object = hashlib.sha1(seq.encode('utf-8'))
                    # The SHA1 digest will always be 40 characters.
                    sha1_digest = hash_object.hexdigest()
                    if(header in self.data):
                        dict_hash = self.data[header]["sha1"]
                        # Diff hash means diff seqs
                        if(dict_hash != sha1_digest and not duplicates_allowed):
                            print("ERROR: Identical headers found for two "
                                  "different sequences.")
                            print("\tFile: " + fasta_file)
                            print("\tViolating header:", header)
                            print("\tIdentical headers are only allowed "
                                  "for identical sequences.")
                            quit(1)
                        elif(dict_hash != sha1_digest and duplicates_allowed):
                            self.duplicates[header] = True
                        else:
                            self.duplicates[header] = True

                    self.data[header] = {"sha1": sha1_digest,
                                         "length": len(seq),
                                         "file": fasta_file}
        elif(json_file):
            with open(json_file, "r", encoding="utf-8") as json_fh:
                self.data = json.load(json_fh)

    def dump_json(self, output):
        '''
        '''
        with open(output, 'w', encoding="utf-8") as json_fh:
            json.dump(self.data, json_fh)
